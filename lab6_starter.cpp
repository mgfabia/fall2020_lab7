/**************************
 *Mark Fabian
 *CPSC 1021, section 3, F20
 *mgfabia@clemson.edu
 *Elliot 
 **************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

typedef struct Employee{
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) { return rand()%i;}


int main(int argc, char const *argv[]) {
  
  srand(unsigned (time(0)));


  /*array of 10 employees*/
  Employee arr[10];

  //prompts user for 10 names
  cout << "Enter 10 employees starting with their first name, last name," 
	"birth year, and hourly wage" << endl;

  //loops for 10 employee entries 
  for(int k = 0; k<10;k++){
      cout << "First Name: " << endl;
      cin >> arr[k].firstName;
      cout << "Last Name: " << endl;
      cin >> arr[k].lastName;
      cout << "Birth Year: " << endl;
      cin >> arr[k].birthYear;
      cout << "Hourly Wage: " << endl;
      cin >> arr[k].hourlyWage; 
  }

  /*function shuffles array*/
   random_shuffle(arr, arr+10, myrandom);

   /*Smaller array*/
   Employee fiveEmployees[5];

   for(int j=0;j<5;j++){
       fiveEmployees[j] = arr[j];
   }

    /*Sort the new array. */
    sort(fiveEmployees, fiveEmployees+5, name_order);

    /*Prints array of 5 employees */

   for(auto x: fiveEmployees) {
    cout << x.lastName << setw(10) <<  " " <<  x.firstName << " " << 
    x.birthYear << " " << x.hourlyWage <<  endl;
   }


  return 0;
}


/*this function compairs last name and first name */
bool name_order(const employee& lhs, const employee& rhs) {
  
  if(lhs.lastName != rhs.lastName){
    return lhs.lastName < rhs.lastName;
  }
  else
  {
    return lhs.firstName < rhs.firstName;
  }
}

